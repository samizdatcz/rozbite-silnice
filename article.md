---
title: "Mapa: Kolik nehod zaviní zanedbané silnice?"
perex: "Český rozhlas analyzoval údaje z policejní databáze dopravních nehod. Podívejte se, kolik havárií způsobí &bdquo;závada komunikace&ldquo;, a zapojte se do hlasování o nejhorší výmol v republice."
description: "Český rozhlas analyzoval údaje z policejní databáze dopravních nehod. Podívejte se, kolik havárií způsobí &bdquo;závada komunikace&ldquo;."
authors: ["Luděk Hubáček", "Lenka Rezková", "Petr Kočí", "Jan Cibulka"]
published: "6. března 2016"
socialimg: https://interaktivni.rozhlas.cz/rozbite-silnice/media/mapa.png
# coverimg: http://www.renoznacky.cz/images/stories/virtuemart/product/a07a.png
# coverimg_note: "Foto <a href='#'>ČTK</a>"
# url: "brexit"
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
libraries: [jquery]
recommended:
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/tri-roky-modernizace-d1-kde-to-nejvic-drnca--1559167
    title: Tři roky modernizace D1: Kde to nejvíc drncá?
    perex: Český rozhlas změřil intenzitu otřesů při jízdě po nejděravější české dálnici. Projeďte si ji kilometr po kilometru v interaktivní mapě a podívejte se, jak to hází na nejzanedbanějších úsecích a jak na nově opravených.
    image: http://media.rozhlas.cz/_obrazek/3721130--dalnice-d1-jihlava--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/technika/_zprava/stoleti-brouka-hitlerovo-lidove-auto-prezilo-valku-a-svet-si-ho-zamiloval--1584151
    title: Století Brouka
    perex: Hitlerovo lidové auto přežilo válku a svět si ho zamiloval
    image: https://samizdat.cz/data/volkswagen-80/www/media/cover.jpg
---

<aside class="small">
  <figure>
    <img src="http://www.renoznacky.cz/images/stories/virtuemart/product/a07a.png" width="300">
  </figure>
  <figcaption>
    Dopravní značka A07a Nerovnost vozovky
  </figcaption>
</aside>


V úterý 15. listopadu se prudce zhoršilo počasí. Jablonečtí hasiči dostali hlášení o osobním autě, které se skutálelo ze stráně u obce Rádlo. Cestou k nehodě ale zásahová cisterna Tatra dostala smyk a sama se [převrátila na bok](http://www.pozary.cz/clanek/150217-u-obce-radlo-doslo-k-nehode-zasahove-cisterny-jabloneckych/). [Dva z hasičů se zranili](http://www.hzslk.cz/56.5264-radu-komunikaci-pokryl-led.html) a ze zásahového vozidla vyteklo palivo. Z policejní [databáze dopravních nehod](http://pcr.jdvm.cz/pcr/), kterou analyzoval Český rozhlas, vyplývá, že šlo o nehodu s jednou z nejvyšších zaznamenaných hmotných škod.

Za posledních deset let se v Česku stalo 3480 nehod, které podle policie nezavinil žádný z řidičů, ale _závada komunikace_: rozbitá silnice, prasklá kanálová vpusť, přerostlé křoví u krajnice, chybějící nebo poškozené dopravní značky nebo třeba neuklizený uježděný sníh.

<aside class="small">
  <h3>
    Nedá se tam jet, je to území nikoho
  </h3>
  <figure>
    <audio src="https://samizdat.blob.core.windows.net/zvuky/silnice-rybi.mp3" controls="controls"></audio>
  </figure>
  <figcaption>
    Nevyjasněné majetkové poměry brání opravě silnice u obce Rybí na Ostravsku. Poslechněte si reportáž Michala Poláška
  </figcaption>
</aside>

&bdquo;_Policie vždy posuzuje, jestli řidič mohl předpokládat stav této komunikace, jestli byl výmol označen, anebo naopak byla silnice bezproblémová a najednou se v ní objevila náhlá závada_,&ldquo; vysvětluje šéf dopravní policie Tomáš Lerch kritéria pro označení špatného stavu silnice za viníka nehody.


Při takových nehodách se za posledních deset let zranilo 13 lidí těžce, lehce 129; nikdo nezahynul. Škody se přiblížily 900 milionům korun. Všechny nehody zaviněné v letech 2007 až 2016 špatným stavem silnic najdete v následující mapě. Čím je kolečko větší, tím větší vznikla při nehodě škoda. Červená barva signalizuje nehody, při kterých se navíc někdo zranil. 

<div data-bso="1"></div>

<aside class="big">
  <figure>
    <iframe src="https://interaktivni.rozhlas.cz/data/nehody-diry-mapa/" class="ig" width="1024" height="580" scrolling="no" frameborder="0"></iframe>
  </figure>
  <figcaption>
    Zdroj dat: <a href="http://pcr.jdvm.cz/pcr/">Policie ČR</a>
  </figcaption>
</aside>

Ačkoli se jich za deset let stalo přes tři tisíce, tedy zhruba jedna za den, jsou nehody přímo zaviněné špatně udržovanými silnicemi vlastně výjimečné: za stejnou dobu totiž policie zaevideovala různých nehod přes milion. Nehody zobrazené v mapě tak tvoří jen 0,35 procenta ze všech, které se za tu dobu staly. Pro porovnání: havárie zaviněné poruchou na autě jsou o polovinu častější a těch, které zaviní lesní či jiná zvěř, je dokonce dvacetkrát víc. 

Policisté zaznamenávají stav vozovky u každé nehody, nejen u těch závadou komunikace přímo zaviněných. Nesouvislé výtluky našli za deset let u 6660 havárií, souvislé výtluky u 1813, zvlněný povrch v podélném směru u 1625 a nesprávně umístěnou, znečištěnou nebo chybějící značku u 930 nehod.

## Hlasování o nejhorší výmol začíná

&bdquo;_V roce 2016 Policie České republiky nahlásila správcům komunikací bezmála 15 tisíc závad,_&ldquo; řekl Českému rozhlasu šéf dopravní policie Tomáš Lerch. &bdquo;_Z toho zhruba dva tisíce tvořily závady povrchu komunikace, tedy výmoly, vyjeté koleje, větší praskliny..._&ldquo; Asi polovinu z nich dokázali silničáři podle policejní evidence ještě v loňském roce opravit.

Poškozené silnice můžou silničářům hlásit také samotní řidiči, například pomocí [webové stránky a mobilní aplikace Výmoly.cz](https://www.vymoly.cz/). Za rok 2016 podle jejího provozovatele Petra Čaníka motoristé nahlásili přibližně 1300 děr a silničářům se jich podařilo opravit o něco víc než třetinu. Je to i díky tomu, že webová stránka každý den brzy ráno automaticky provede inventuru nově nahlášených výmolů a odešle jejich přehled úředníkům, kteří se mají o danou silnici starat.  

&bdquo;_Řidiči v naší aplikaci nově najdou také například informaci o tom, které úseky silnic se budou brzy důkladněji opravovat, a proto se tam nyní provádějí jen provizorní opravy nechvalně známé jako kobylince. Pro silničáře není efektivní pokoušet se o důkladnější opravu, když vědí, že za půl roku bude celý úsek pokryt novou vrstvou asfaltu,_&ldquo; vysvětluje provozovatel webu Výmoly.cz Čaník.

V pondělí 6. března zahajuje společně se Zelenou vlnou a Radiožurnálem další ročník ankety o nejhorší výmol v republice. Loni [vyhrála v hlasování více 100 tisíc řidičů silnice číslo 259 na Českolipsku](http://www.rozhlas.cz/zelenavlna/portal/_zprava/foto-letos-ne-u-seradiste-ale-u-hradu-houska-ridici-vybrali-nejhorsi-vymol-v-cesku--1641522).

<aside class="big">
  <figure>
    <iframe src="https://www.vymoly.cz/widget/cz/" width="1024" height="700" frameborder="0" scrolling="no"></iframe>
  </figure>
</aside>


## Nejhorší bývá jaro

<aside class="small">
  <h3>
    Finišer a žehlička
  </h3>
  <figure>
    <audio src="https://samizdat.blob.core.windows.net/zvuky/silnice-technologie.mp3" controls="controls"></audio>
  </figure>
  <figcaption>
    Jaké jsou výhody a nevýhody různých technologií pro opravu rozbité silnice? Poslechněte si reportáž Luďka Hubáčka
  </figcaption>
</aside>


Podle zkušností řidičů i policistů bývají silnice nejhorší na jaře. Ještě na nich zůstávají zbytky posypů, to je nebezpečné hlavně pro motorkáře. A voda, která nateče do prasklin v silnici a v zimě zmrzne, může asfaltu napáchat rozsáhlé škody. &bdquo;_Led má obrovskou rozpínavou sílu a když zateče i jen mikrotrhlinami do vozovky, začne trhat, ať mu v cestě stojí asfalt nebo beton,_&ldquo; říká Jan Rýdl z Ředitelství silnic a dálnic. 

Končící zima přitom [patřila mezi nejchladnější za několik desetiletí](https://interaktivni.rozhlas.cz/prazska-zima/). &bdquo;_Monitoring stavu silnic děláme průběžně celý rok. Nicméně po silnější zimě jako byla ta letošní, je poškození větší,_&ldquo; konstatuje Rýdl z ŘSD. Na opravy více než šesti tisíc kilometrů dálnic a silnic prvních tříd má jeho firma na letošek v rozpočtu 10 miliard korun.  

## Odškodnění? Případ od případu

V jakém případě mají řidiči nárok na odškodnění, když si poničí auto o díru ve vozovce, a kdy naopak ne? Záleží na tom, kde se díra v silnici nachází.

&bdquo;_Posuzuje se například, jestli byla v daném úseku varovná značka nebo značka omezující rychlost. Pokud tam žádná značka není a řidič tudíž nemohl předpokládat, že se výmol objeví, je velká pravděpodobnost, že k náhradě za škody dojde,_&ldquo; říká mluvčí Technické správy komunikaci Barbora Lišková.

Firma, která spravuje pražské ulice a silnice přitom loni řidičům za poškozená auta zaplatila 660 tisíc korun.